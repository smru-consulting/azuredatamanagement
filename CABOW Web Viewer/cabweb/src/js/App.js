import '../css/styles.css';
import React, { Component } from 'react';
import BuoyList from './Buoys/BuoyList';
import DataList from './Display/DataList';
import DataOverviewVis from './Display/DataOverviewVis';
import RecordingQueueDisplay from './Display/RecordingQueueDisplay';
import DetectionsView from './Display/DetectionsView';

class App extends React.Component {

    constructor(props){
        super(props);
        this.state= {user:{name:"SMRUadmin",key:"doesntmatter"},dataList:[]};
    }

    addDataView = (newView) => {
        let exists = false;
        for(const [idx,elem] of this.state.dataList.entries()){
            for(const [key,value] of Object.entries(elem)){
                if(key!="key" && newView[key]==value){
                    exists=true;
                }else if(key!="key"){
                    exists=false;
                }
            }
            if(exists==true){
                break;
            }
        }

        if(exists==false){
            let key=this.state.dataList.length;
            newView["key"]=key;
            this.setState(prevState => ({
                dataList:[...prevState.dataList, newView]
            }));
        }
        
    }

    removeDataView = (DOMLabel) =>{
        console.log("Called the big guy "+DOMLabel);
        this.setState({dataList: this.state.dataList.filter(function(dataView) { 
            return dataView["DOMLabel"] !== DOMLabel 
        })});
    }

    render(){
        let list = this.state.dataList.map((dataView) => {
            if (dataView["displayType"]=="DataOverviewVis"){
                let label = dataView["buoyMeta"]["ID"]+" Overview";
                return <DataOverviewVis key={dataView["key"]} label={label} user={this.state.user} value={dataView["DOMLabel"]} buoyMeta={dataView["buoyMeta"]} removeThis={(DOMLabel) => this.removeDataView(DOMLabel)}/>
            } else if (dataView["displayType"]=="DetectionView"){
                let label = dataView["buoyMeta"]["ID"]+" Detections";
                let defaultTimeBounds = {start:"2021-11-10 00:58:11.059",end:"2021-12-30 00:58:11.059"};
                return <DetectionsView key={dataView["key"]} label={label} user={this.state.user} value={dataView["DOMLabel"]} buoyMeta={dataView["buoyMeta"]} removeThis={(DOMLabel) => this.removeDataView(DOMLabel)} timeBounds={defaultTimeBounds}/>
            }
        })
        return(
            <div>
                <div className="activeDeployments">
                    <BuoyList addDataView={this.addDataView} user={this.state.user}/>
                </div>
                <div className="dataDisplay">
                    <DataList user={this.state.user}>
                    {list}
                    </DataList>
                </div>
            </div>
        )
    }

}

export default App;
