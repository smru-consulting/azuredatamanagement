import '../../css/styles.css';
import React, { Component } from 'react';
import BuoyPopup from './BuoyPopup';
import $ from 'jquery';


class Buoy extends React.Component {
    constructor(props) {
      super(props);
      this.state = {details: false,options:false,buoyMeta:props.buoyMeta};
    }

    toggleShowOptions(){
        if(this.state.options){
            this.setState({ options: false });
            //$("#"+this.buoyMeta.ID+".buoyPopup").remove();
        }else{
            this.setState({ options: true })

        }

    }

    optionspopup(props){
        if(props.show==true){
            return(
                <div>
                    <BuoyPopup className="buoyPopup" id={props.buoyMeta.ID} show={props.show} buoyMeta={props.buoyMeta} addDataView={props.addDataView}></BuoyPopup>
                </div>
            );
        }else{
            $("#"+props.buoyMeta.ID+".buoyPopup").remove();
            return null;
        }
    }

    
  
    render() {

      return (
          <div className="buoy" id={this.state.buoyMeta.ID}>
            <div className="buoyHeading" id={this.state.buoyMeta.ID}>
                <h3>Buoy {this.state.buoyMeta.ID}: </h3><p>{this.state.buoyMeta.LocationName}</p>
                <button onClick={() => this.toggleShowOptions()}>
                    Show options
                </button>
                <this.optionspopup show={this.state.options} addDataView={this.props.addDataView} buoyMeta={this.state.buoyMeta}>{this.state.options}</this.optionspopup>
            </div>
          </div>
        );
    }
  }

  export default Buoy;