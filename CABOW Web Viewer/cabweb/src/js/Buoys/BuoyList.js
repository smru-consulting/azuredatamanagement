import '../../css/styles.css';
import React, { Component } from 'react';
import Buoy from './Buoy';
import $ from 'jquery';
import axios from 'axios';



class BuoyList extends React.Component {
    constructor(props) {
      super(props);
      this.state = {user: props.user,activeBuoyList:null};
    }

    componentDidMount(){
      axios({
        method: 'post',
        url: "http://localhost:8000/php/BuoyData/Meta/AccessActiveBuoys.php?",
        data:JSON.stringify(this.state.user),
      }).then(res => this.setState({ activeBuoyList: res.data }));
      
    }

    render(){
      console.log("ACTIVE BUOY LIST");
      console.log(this.state.activeBuoyList)
      if(this.state.activeBuoyList==null){
        return(<div></div>)
      }
      else{
        var buoys = [];
        let idx=0;
        for (const buoyKey in this.state.activeBuoyList) {
          buoys.push(<Buoy key={idx} addDataView={this.props.addDataView} buoyMeta={this.state.activeBuoyList[buoyKey]}/>);
          idx++;
        } 
        return(
                 <div>{buoys}</div>
        );
      }
    }
    

    

}
export default BuoyList;