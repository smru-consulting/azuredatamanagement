import React, { Component } from 'react';
import DataOverviewVis from '../Display/DataOverviewVis';


class BuoyPopup extends React.Component{
        constructor(props){
        super(props);
      this.state = {show:props.show,buoyMeta:props.buoyMeta};
    }

    createDataOverviewTab(buoyMeta){
        this.props.addDataView({"displayType":"DataOverviewVis","buoyMeta":buoyMeta,"DOMLabel":"DataOverviewVis"+buoyMeta.ID});
    }

    createRecordingQueueDisplay(buoyMeta){
        this.props.addDataView({"displayType":"RecordingQueueDisplay","buoyMeta":buoyMeta,"DOMLabel":"RecordingQueueDisplay"+buoyMeta.ID});
    }

    createDetectionView(buoyMeta){
        this.props.addDataView({"displayType":"DetectionView","buoyMeta":buoyMeta,"DOMLabel":"DetectionView"+buoyMeta.ID});
    }
    
    render(){
        if(this.state.show){
            return(
                <div className="buoyShowOptionsPopup">
                    <button onClick={() => this.createDataOverviewTab(this.state.buoyMeta)}>Open Data Overview</button>
                    <button onClick={() => this.createDetectionView(this.state.buoyMeta)}>Open Detection Data</button>
                    
                </div>
            );
        }else{
            return null
        }
        
    }


}

export default BuoyPopup;