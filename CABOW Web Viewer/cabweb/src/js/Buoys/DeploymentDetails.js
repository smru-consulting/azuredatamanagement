import '../../css/styles.css';
import React, { Component } from 'react';

class DeploymentDetails extends Component{
    constructor(props){
        super(props);
        this.state = {buoyMeta:props.buoyMeta};
    }

    render(){
        console.log(this.state.buoyMeta)
        return(
            <div>
                <tr>
                    <td>Location Name</td>
                    <td>{this.state.buoyMeta.LocationName}</td>
                </tr>
                <tr>
                    <td>Latitude</td>
                    <td>{this.state.buoyMeta.lat}</td>
                </tr>
                <tr>
                    <td>Longitude</td>
                    <td>{this.state.buoyMeta.lon}</td>
                </tr>
                <tr>
                    <td>Detectors</td>
                    <td>Killer Whale</td>
                </tr>
            </div>
        )
    }



}

export default DeploymentDetails