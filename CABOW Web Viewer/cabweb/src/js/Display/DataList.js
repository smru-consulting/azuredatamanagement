import '../../css/styles.css';
import React, { Component } from 'react';
import DataVIsDisplay from './DataVisDisplay';

class DataList extends React.Component {

    
      constructor(props) {
        super(props);
    
        this.state = {
          activeTab: null,
        };
      }

      componentDidUpdate(previousProps, previousState){
        if(previousProps.children.length!=this.props.children.length){
          if(this.props.children.length>0){
            let newTab = this.props.children[this.props.children.length-1].props.value;
             this.setState({activeTab: newTab});
           }
        }
      }

      changeTab = (value) => {
        this.setState({activeTab:value})
      }

      render(){
    
        return (
          <div className="tabs">
            <ul className="tab-list">
            {this.props.children.map((child) => {
              let activeTab = "notactive";
              if(child.props.value==this.state.activeTab){
                activeTab = "active";
              }
              return <li className={"tab "+activeTab} key={child.key} onClick={() => this.changeTab(child.props.value)}><a>{child.props.label}</a></li>
            })}
            </ul>
            <div className="tab-content">
              {this.props.children.map((child) => {
                if(child.props.value==this.state.activeTab){
                  return child;
                }
              })}
            </div>
          </div>
        );
      }

}

export default DataList;