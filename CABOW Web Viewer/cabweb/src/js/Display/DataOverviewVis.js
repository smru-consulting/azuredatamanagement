import React, { Component } from 'react';
import DataVIsDisplay from './DataVisDisplay';
import jsonp from 'jsonp';
import {XYPlot,XAxis,YAxis,VerticalGridLines,HorizontalGridLines,MarkSeries,makeWidthFlexible,Hint} from 'react-vis';
import RecordingQueueDisplay from './RecordingQueueDisplay';
import DeploymentDetails from '../Buoys/DeploymentDetails';
import '../../../node_modules/react-vis/dist/style.css';


class DataOverviewVis extends DataVIsDisplay{
    constructor(props){
        super(props);
        this.state={buoyMeta:props.buoyMeta,dataSets:[],user:props.user,value:{}}
    }

    componentDidMount(){
        var ID = this.state.buoyMeta["ID"];
        for(var i=0;i<this.props.buoyMeta.dTypes.length;i++){
            var type = this.state.buoyMeta.dTypes[i];
            var dataURL = `http://localhost:8000/php/BuoyData/Meta/HighLevelBuoyData.php?buoyID=${ID}&dataType=${type}&callback=parse${type}`;
            var config = {
                headers: {
                  'content-type': 'application/javascript'
                }
            }
            jsonp(dataURL, {
                method: 'post',
                data:JSON.stringify(this.state.user),
                name:'parse'+type,
                headers:config.headers
              },(err,data) => {
                //this.initTimerange(data);
                console.log(data)
                this.setState(prevState => ({
                    dataSets:[...prevState.dataSets, data]
                }));
            })
            
        }
    }

    showDataPlots(data,key){

        var xyplots = data[0].series.map((aSeries) => {
            var xydata = aSeries.data.map((data) => {
                var dateStr = new Date(data[0]);
                return({x:dateStr,y:data[1]})
            })
            return{name:aSeries.name,
                    data:xydata} 
        });
        const FlexibleXYPlot = makeWidthFlexible(XYPlot); 
        console.log(this.state);
        return( 
            <FlexibleXYPlot key={key} height={"300"} xType={"time"} onMouseLeave={()=>{this.setState({value:false})}}>
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis tickTotal={5}/>
                <YAxis  />
                {xyplots.map((set,i)=>{
                    return(
                        <MarkSeries
                        className={set.name}
                        strokeWidth={1}
                        opacity="0.8"
                        key={i}
                        sizeRange={[10, 50]}
                        data={set.data}
                        onNearestXY = {(value) => this.setState(prevState => {
                                                                return{
                                                                    ...prevState,
                                                                    value:{...prevState.value,
                                                                    [i]:value}
                                                                }})}
                        />
                    )}
                )}
                {Object.keys(this.state.value).length > 0 ? <Hint value={{time:this.state.value[key].x.toLocaleDateString('en-us', {year:"numeric", month:"numeric", day:"numeric",hour:"numeric"}),value:this.state.value[key].y} } /> : null}
          </FlexibleXYPlot>)
    }

    getDataDisplay(){
        var dataVis
        if(this.state.dataSets.length>0){
            dataVis = this.state.dataSets.map((set,i) => {
                return(
                <div key={i}>
                    {this.showDataPlots(set,i)}
                </div>
                )  
            })
        }else{
            dataVis = null;
        }

        return(
            <div>
                <div className="overview-vis">
                    {dataVis}
                </div>
                <div className="deploymentDetails-display">
                    <DeploymentDetails buoyMeta={this.state.buoyMeta}/>
                </div>
                <div className="request-display">
                    <RecordingQueueDisplay user={this.state.user} buoyMeta={this.state.buoyMeta}/>
                </div>
            </div>
        )
    }

    getDataType(){
        return(<p>Data Overview</p>);
    }
}

export default DataOverviewVis;