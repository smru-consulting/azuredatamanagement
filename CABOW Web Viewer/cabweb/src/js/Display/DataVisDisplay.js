import '../../css/styles.css';
import React, { Component } from 'react';

class DataVIsDisplay extends React.Component {

    constructor(props){
        super(props);
        this.state = {buoyMeta:props.buoyMeta,user:props.user};
        //console.log("BUOY META"+props.buoyMeta)
    }

    getDataDisplay(){
        return(
            null
        )
    }

    getDataType(){
        return <p>Parent</p>;
    }

    removeThis = (DOMValue) => {
        this.props.removeThis(DOMValue);
    }

    displayChart(data){
        return <p>Called the parents</p>
    }

    render(){
        return(
        <div className="dataVis">
            <div className="dataHeader">
                {this.state.buoyMeta["ID"]} 
                {this.getDataType()}
                <button onClick={() => this.removeThis(this.props.value)}>Close</button>
            </div>
            <div className="dataContent">
                {this.getDataDisplay()}
            </div>
            
        </div>)
    }

}

export default DataVIsDisplay;