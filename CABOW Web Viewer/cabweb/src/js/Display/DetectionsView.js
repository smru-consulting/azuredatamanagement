import React, { Component } from 'react';
import DataVIsDisplay from './DataVisDisplay';
import {XYPlot,XAxis,YAxis,VerticalGridLines,HorizontalGridLines,LineSeries,makeWidthFlexible,Hint,RadialChart,ChartLabel,CircularGridLines,Highlight} from 'react-vis';
import RecordingQueueDisplay from './RecordingQueueDisplay';
import '../../../node_modules/react-vis/dist/style.css';
import axios from 'axios';
import '../../css/styles.css';



class DetectionsView extends DataVIsDisplay{
    constructor(props){
        super(props);
        this.state={buoyMeta:props.buoyMeta,detections:[],user:props.user,timeBounds:props.timeBounds,bearingHover:false,lastDrawLocation:null,interestDet:null}
    }

    componentDidMount(){
        var ID = this.state.buoyMeta["ID"];
        let detectionsURL = `http://localhost:8000/php/BuoyData/Pamguard/Detections.php?buoyID=${ID}`
        axios.post(detectionsURL,JSON.stringify(this.state.timeBounds)).then((result) => {
            this.setState({detections:result.data})
        })
    }

    displayAllDetsOneTimeline(allDets){
        const FlexibleXYPlot = makeWidthFlexible(XYPlot); 
        let naturalMin = new Date(allDets[0].tmin);
        return(<div className="timeline">
                <FlexibleXYPlot  
                margin={{left: 70,right:70}} 
                xDomain={this.state.lastDrawLocation && [this.state.lastDrawLocation.left, this.state.lastDrawLocation.right]} 
                yDomain={this.state.lastDrawLocation ? [this.state.lastDrawLocation.bottom, this.state.lastDrawLocation.top] : [0,40000]}
                height={300} 
                xType="time" >
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis  />
                <Highlight
                onBrushEnd={(area) => {
                    this.setState(
                    { lastDrawLocation: area }
                    )
                }}
                />
                {allDets.map((detection,idx)=>{
                console.log(detection)
                return(
                    <LineSeries key={idx}
                    data={detection.lowTimeSeries.data}
                    style={{ fill: 'none' }}
                    color={1}
                    color={this.state.interestDet==idx ? 'blue' : 'red'}
                    />
                )})}
                 {allDets.map((detection,idx)=>{
                console.log(detection)
                return(
                    <LineSeries key={idx}
                    data={detection.highTimeSeries.data}
                    style={{ fill: 'none' }}
                    color={this.state.interestDet==idx ? 'blue' : 'red'}
                    />
                )})}
                <ChartLabel
                    text={this.state.lastDrawLocation ? this.state.lastDrawLocation.left.toString() : naturalMin.toString()}
                    includeMargin={false}
                    xPercent={0.01}
                    yPercent={0.15}
                    />
            </FlexibleXYPlot>
            
            
        </div>)
    }

    displayAllDetsDifTimelines(allDets){
        const FlexibleXYPlot = makeWidthFlexible(XYPlot); 
        return(<div className = "detailed-detections">
            {allDets.map((detection,idx)=>{
                let tminString = new Date(detection.tmin);
                return(
                <div className="detectioncontainer" ><div className="detection">
                <FlexibleXYPlot 
                onMouseEnter={()=>{this.setState({interestDet:idx})}}
                onMouseLeave={()=>{this.setState({interestDet:null})}}
                className="detectioncontour" 
                key={idx}  
                margin={{left: 70,right:70}} 
                height={300} 
                width={500} 
                xType="time" 
                xDomain={[detection.tmin-500,detection.tmin+500]}
                yDomain={[detection.xmin-500,detection.xmax+500]}>
                <VerticalGridLines />
                <HorizontalGridLines />
                <XAxis />
                <YAxis  />
                
                    <LineSeries
                    data={detection.lowTimeSeries.data}
                    style={{ fill: 'none' }}
                    color={this.state.interestDet==idx ? 'blue' : 'red'}
                    />
                    <LineSeries
                    data={detection.highTimeSeries.data}
                    style={{ fill: 'none' }}
                    color={this.state.interestDet==idx ? 'blue' : 'red'}
                    />
                    
                    <ChartLabel
                    text={tminString.toString()}
                    includeMargin={false}
                    xPercent={0.01}
                    yPercent={0.15}
                    />
            </FlexibleXYPlot></div><div className="bearing">  
            {this.displayBearings(detection)}</div></div>)
            
            })}
        </div>)
    }

    displayDetectionFrequency(detections){
        let plotOneTimeline = null;
        let plotManyTimelines = null;
        if(detections.length>0){
            var allDets = detections.map((detection) => {
                var lowTimeSeries = {data:detection.timeArray.map((time,idx) => {
                    return({x:new Date(time),y:detection.lowFreqs[idx]});
                }),name:'lowFreqs'};
                var highTimeSeries = {data:detection.timeArray.map((time,idx) => {
                    return({x:new Date(time),y:detection.highFreqs[idx]});
                }),name:'highFreqs'};
                console.log(lowTimeSeries);
                console.log("MIN: "+Math.min.apply(null,detection.timeArray));
                return{lowTimeSeries:lowTimeSeries,highTimeSeries:highTimeSeries,tmin:Math.min.apply(null,detection.timeArray),theta:detection.bearing0,xmin:Math.min(detection.highFreqs),xmax:Math.max(detection.highFreqs)}
            })
            console.log(allDets);
            
            plotOneTimeline = this.displayAllDetsOneTimeline(allDets);
            plotManyTimelines = this.displayAllDetsDifTimelines(allDets);
        
            }
        return( <div>{plotOneTimeline}{plotManyTimelines}</div>)
    }

    displayBearings(detection){
        let plot = null;
        let bearing = this.state.buoyMeta["thetaOff"]+detection.theta;
        console.log("BEARING: "+bearing+" buoyMeta ");
        console.log(this.state.buoyMeta);
        let data=[{r:0,theta:bearing+Math.PI/2},{r:5,theta:bearing+Math.PI/2}];
        let north=[{r:0,theta:0+Math.PI/2},{r:5,theta:0+Math.PI/2}]
        let FlexibleXYPlot = makeWidthFlexible(XYPlot);
        let boundAngleDeg = detection.theta*180/Math.PI;
        while(boundAngleDeg<0){
            boundAngleDeg+=360;
        }
        boundAngleDeg = Math.round(boundAngleDeg);
        plot = 
        (<FlexibleXYPlot 
            onMouseEnter={()=>{this.setState({bearingHover:true})}}
            onMouseLeave={()=>{this.setState({bearingHover:false})}}
            xDomain={[-3, 3]}
            yDomain={[-3, 3]}
            height={100}
            >
            <CircularGridLines />
            
            <LineSeries
                strokeWidth={2}
                
                data={data.map(row => ({
                ...row,
                x: -1*Math.cos(row.theta) * row.r,
                y: Math.sin(row.theta) * row.r
                }))}/>
                {this.state.bearingHover ? <Hint value={{Bearing:boundAngleDeg+"\xB0"}}/> : null}
                <LineSeries
                strokeWidth={2}
                strokeStyle ={"dashed"}
                data={north.map(row => ({
                ...row,
                x: -1*Math.cos(row.theta) * row.r,
                y: Math.sin(row.theta) * row.r
                }))}/>
                <ChartLabel
                        text={"N"}
                        includeMargin={true}
                        xPercent={0.5}
                        yPercent={-.3}
                        />
            </FlexibleXYPlot>);
        
    return(<div>{plot}</div>);
    }

    getDataDisplay(){
        return(<div><div >{this.displayDetectionFrequency(this.state.detections)}</div>
       </div>)
    }

    getDataType(){
        return(<p>Detections</p>);
    }

}

export default DetectionsView;