import React, { Component } from 'react';
import DataVIsDisplay from './DataVisDisplay';
import {XYPlot,XAxis,YAxis,VerticalGridLines,HorizontalGridLines,MarkSeries,makeWidthFlexible} from 'react-vis';
import axios from 'axios';


class RecordingQueueDisplay extends Component{
    constructor(props){
        super(props);
        this.state={buoyMeta:props.buoyMeta,queue:null,user:props.user,showRequestForm:false,showBadRequest:false,date:'',startTime:'',endTime:''}
    }

    componentDidMount(){
        var ID = this.state.buoyMeta["ID"];
        const dataURL = `http://localhost:8000/php/BuoyData/Recordings/RecordingQueueAccess.php?buoyID=${ID}`;
        console.log(dataURL)
        var config = {
            headers: {
                'content-type': 'application/javascript'
            }
        }
        axios.get(dataURL)
        .then((response) => {
            this.setState({queue:response.data});
        })
            
        
    }

    showQueue(key){
        let queue = null;
        if(this.state.queue!=null){
             queue = this.state.queue.requests.request.map((request) =>{
                return(<tr key={key}>
                        <td>{request["@attributes"].dateStr}</td>
                        <td>{request["@attributes"].startTime}</td>
                        <td>{request["@attributes"].endTime}</td>
                        <td>{request["@attributes"].staged}</td>
                       </tr>)
            })
        }
        return(
            <table>
                <tr>
                    <th>Date</th>
                    <th>Start Time</th>
                    <th>End Time</th>
                    <th>Staged</th>
                </tr>
                {queue}
          </table>
        )
    }

    toggleRequestForm(){
        if(this.state.showRequestForm){
            this.setState({showRequestForm:false});
        }else{
            this.setState({showRequestForm:true})
        }
    }

    postFormAndHide(){
        var ID = this.state.buoyMeta["ID"];
        const dataURL = `http://localhost:8000/php/BuoyData/Recordings/RecordingQueueAccess.php?buoyID=${ID}`;
        let data={dateStr:this.state.date,startTime:this.state.startTime,endTime:this.state.endTime,staged:0};
        console.log(data);
        axios.post(dataURL,JSON.stringify(data))
        .then((result) => {
            if(result.data=='fail'){
                this.setState({showBadRequest:true});
            }else{
                this.componentDidMount();
                
            }
        });
        this.setState({showRequestForm:false})
    }

    formChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
      }

    getDataDisplay(){
        //const { date, startTime, endTime } = this.state.request;
        let requestForm = null;
        if(this.state.showRequestForm){
            requestForm = <div>
                            <p>Request Form</p>
                            <form onSubmit={()=>this.postFormAndHide()}>
                                <label htmlFor="date">Date String:</label>
                                <input type="text" id="date" name="date" value={this.state.date} onChange={this.formChange}/><br/>
                                <label htmlFor="startTime">Start Time:</label>
                                <input type="text" id="startTime" name="startTime" value={this.state.startTime} onChange={this.formChange}/><br/>
                                <label htmlFor="endTime">End Time:</label>
                                <input type="text" id="endTime" name="endTime" value={this.state.endTime} onChange={this.formChange}/><br/>
                                <button type="submit" value="Submit">Submit</button>
                            </form>
                          </div>
        }
        let invalidRequest =null;

        if(this.state.showBadRequest){
            invalidRequest = <div>Invalid request</div>;
        }

        return(
            <div>
                {this.showQueue()}
                {invalidRequest}
                <button onClick={() => this.toggleRequestForm()}>Request Form</button>
                {requestForm}
            </div>
        )
    }

    getDataType(){
        return <p>Recording Queue</p>;
    }

    render(){
        return(<div>{this.getDataDisplay()}</div>);
    }
}

export default RecordingQueueDisplay;