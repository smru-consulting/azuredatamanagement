from flask import Flask, request,jsonify
from flask_restful import reqparse,Resource, Api
import json

activeBuoysJsonLoc = "C:/AzureDevelopment/AzureDataManagement/StaticData/sampleJSON/projects/activeBuoys.json"
activeBuoysJsonFile = open(activeBuoysJsonLoc)
activeBuoys = json.load(activeBuoysJsonFile)


class Buoy(Resource):
    def get(self):
        data = json.loads(request.data)
        return jsonify(activeBuoys['activeBuoyList'][data['buoyID']])

    def put(self):
        ID = request.form['ID']
        activeBuoys['activeBuoyList'][ID] = request.form
        return request.form

class BuoyList(Resource):
    def get(self):
        data = json.loads(request.data)
        userBuoys = dict()
        for (buoyID,data) in activeBuoys['activeBuoyList']:
            if data['User'] in data['access']:
                userBuoys[buoyID] = data
        return userBuoys