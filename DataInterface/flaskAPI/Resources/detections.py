from flask import Flask, request,jsonify
from flask_restful import Resource, Api
import sqlite3
import pandas as pd
import json
from datetime import datetime


class DetectionList(Resource):
    def __init__(self):
        self.sqliteLoc = 'C:/AzureDevelopment/AzureDataManagement/StaticData/sampledb/testdb_09.sqlite3'
        self.con = sqlite3.connect(self.sqliteLoc)

    def get(self):
        input = json.loads(request.data)
        buoyMeta = input["buoyMeta"]
        tableName = input['DetectorType']
        startDatetime = datetime.strptime(input['dt_start'], '%Y-%m-%dT%H:%M:%S.%f%z')
        endDateTime = datetime.strptime(input['dt_end'], '%Y-%m-%dT%H:%M:%S.%f%z')
        startTimeStr = startDatetime.strftime("%Y-%m-%d %H:%M:%S.%f")
        endTimeStr = endDateTime.strftime("%Y-%m-%d %H:%M:%S.%f")
        QUERY = '''SELECT UTC FROM '''+tableName+" WHERE UTC BETWEEN \'"+startTimeStr+"\' AND \'"+endTimeStr+"\'"
        df = pd.read_sql_query(QUERY, self.con)
        df['UTC'] = pd.to_datetime(df["UTC"])
        #df = df.set_index("UTC")
        return df.to_json(orient="columns")


class Detection(Resource):
    def __init__(self):
        self.sqliteLoc = 'C:/AzureDevelopment/AzureDataManagement/StaticData/sampledb/testdb_09.sqlite3'
        self.con = sqlite3.connect(self.sqliteLoc)

    def get(self):
        data = json.loads(request.data)
        buoyMeta = data['buoyMeta']
        tableName = data['DetectorType']
        startDatetime = datetime.strptime(data['dt_start'], '%Y-%m-%dT%H:%M:%S.%f%z')
        endDateTime = datetime.strptime(data['dt_end'], '%Y-%m-%dT%H:%M:%S.%f%z')
        startTimeStr = startDatetime.strftime("%Y-%m-%d %H:%M:%S.%f")
        endTimeStr = endDateTime.strftime("%Y-%m-%d %H:%M:%S.%f")
        QUERY = '''SELECT * FROM '''+tableName+" WHERE UTC BETWEEN \'"+startTimeStr+"\' AND \'"+endTimeStr+"\'"
        df = pd.read_sql_query(QUERY, self.con)
        df['UTC'] = pd.to_datetime(df["UTC"])
        df = df.set_index("UTC")
        return df.to_json(orient="columns")