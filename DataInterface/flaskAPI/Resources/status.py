from flask import Flask, request,jsonify
from flask_restful import Resource, Api

import json

activeBuoysJsonLoc = "C:/AzureDevelopment/AzureDataManagement/StaticData/sampleJSON/projects/activeBuoys.json"
activeBuoysJsonFile = open(activeBuoysJsonLoc)
activeBuoys = json.load(activeBuoysJsonFile)


class BuoyStatus(Resource):
    def get(self):
        data = json.loads(request.data)
        return jsonify(activeBuoys['activeBuoyList'][data['buoyID']])

    def put(self):
        ID = request.form['ID']
        activeBuoys['activeBuoyList'][ID] = request.form
        return request.form