from flask import Flask, request
from flask_restful import Resource, Api
import sys
sys.path.append('../')
from Resources import buoys
from Resources import detections

app = Flask(__name__)
api = Api(app)

api.add_resource(buoys.Buoy, '/buoy')
api.add_resource(buoys.BuoyList, '/buoyList')
api.add_resource(detections.DetectionList,'/detectionList')

if __name__ == '__main__':
    app.run(debug=True)