<?php

header('Access-Control-Allow-Origin: *');
header('Accept:application/json');
header('Access-Control-Allow-Methods:GET,HEAD,OPTIONS,POST,PUT');
header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

    $pbID = (string)$_GET['buoyID'];

    class MyDB extends SQLite3 {
        
        function __construct() {
            $docRoot = $_SERVER['DOCUMENT_ROOT'];
            $dbPath = "C:\\AzureDevelopment\\AzureDataManagement\\StaticData\\sampledb\\pb318_PTROB_02.sqlite3";
           $this->open($dbPath);
        }
     }
     $db = new MyDB();

     if($_SERVER['REQUEST_METHOD'] === 'POST'){
        $phpGet = json_decode(file_get_contents('php://input'),true);
        
        $query = "SELECT id,UTC,lowFreq,highFreq,bearing0 FROM MOANS WHERE UTC BETWEEN \"".$phpGet["start"]."\" AND \"".$phpGet["end"]."\"";
        $results= $db->query($query);
        $data=array();
        while ($res= $results->fetchArray(1))
        {
            try{
                $lowFreqStream = $db->openBlob('MOANS', 'lowFreqArray', $res["Id"]);
                $lowFreqByteArray = unpack("E*",stream_get_contents($lowFreqStream));
                
                $highFreqStream = $db->openBlob('MOANS', 'highFreqArray', $res["Id"]);
                $highFreqByteArray = unpack("E*",stream_get_contents($highFreqStream)); 
                $timeStream = $db->openBlob('MOANS', 'timeArray', $res["Id"]);
                $timeByteArray = unpack("J*",stream_get_contents($timeStream));   
            }catch(Exception $e){

            }
            $res["lowFreqs"] = array_values($lowFreqByteArray);
            $res["highFreqs"] = array_values($highFreqByteArray);
            $res["timeArray"] = array_values($timeByteArray);
            fclose($lowFreqStream);
            fclose($highFreqStream);
            fclose($timeStream);
            array_push($data, $res);

        }
        echo json_encode($data);
        $db->close();
    }

     
?> 