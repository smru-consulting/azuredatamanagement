<?php

header('Access-Control-Allow-Origin: *');
header('Accept:application/json');
header('Access-Control-Allow-Methods:GET,HEAD,OPTIONS,POST,PUT');
header('Access-Control-Allow-Headers:Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers');

    $pbID = (string)$_GET['buoyID'];
    $docRoot = $_SERVER['DOCUMENT_ROOT'];
    $queuePath = "C:\\AzureDevelopment\\AzureDataManagement\\StaticData\\deviceData\\".$pbID."\\recordingsRequest\\queue.xml";
    $queueString = file_get_contents($queuePath);
    $queueObj = new SimpleXMLElement($queueString);
    $queueJSON = json_encode($queueObj);
    $queueArray = json_decode($queueJSON);

    if($_SERVER['REQUEST_METHOD'] === 'GET'){
        echo $queueJSON;
    }elseif($_SERVER['REQUEST_METHOD'] === 'POST'){
        $phpPut = file_get_contents('php://input');
        $newRequest = json_decode($phpPut);
        array_push($queueArray->requests->request, $newRequest);
        $request = $queueObj->requests->addChild("request");
        foreach($newRequest as $key => $value){
            $request->addAttribute($key,$value);
        }
        $queueJSON = json_encode($queueObj);
        $success = file_put_contents($queuePath,$queueObj->asXML());
        if($success===false){
            echo "fail";
        }else{
            echo "success";
        }
    }elseif($queueMethod=="DEL"){

    }
?> 