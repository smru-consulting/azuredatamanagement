from os import read
import sys
from PamBinRead import pamBinRead
from readFileHeader import readFileHeader
from readWMData import readWMData
from readWMDataHeader import readWMDataHeader
from readStdModuleFooter import readStdModuleFooter
from readStdModuleHeader import readStdModuleHeader
from readPamData import readPamData
import json

samplePGDF = "C:/Users/12066/Documents/PtRoberts/DatabaseUnpack/CleanEnvironment/clean_raw_bin_dets/WhistlesMoans_Moans_Contours_20211124_012000.pgdf"
samplePGDX = "C:/Users/12066/Documents/PtRoberts/DatabaseUnpack/CleanEnvironment/clean_raw_bin_dets/WhistlesMoans_Moans_Contours_20211124_025400.pgdx"

outputJSON = "C:/AzureDevelopment/AzureDataManagement/StaticData/sampleJSON/projects/DetectionsOut/test.json"

with open(samplePGDF, mode='rb') as file: # b is important -> binary
    fileInfo = {}
    fileInfo['fileName'] = samplePGDF
    fileInfo['readModuleHeader'] = readStdModuleHeader
    fileInfo['readModuleFooter'] = readStdModuleFooter
    prevPos=-1

    while True:
        pos = file.tell()
        #if(pos==prevPos):
         #   break
        prevPos=pos

        nextLen = pamBinRead(file,'int32',n=1)
        nextType = pamBinRead(file,'int32',n=1)
        print(nextType)

        if nextLen==0:
            break
        file.seek(-8,1)
        if nextType==-1:
            fileInfo['fileHeader'] = readFileHeader(file)

            if fileInfo['fileHeader']['moduleType']=="WhistlesMoans":
                fileInfo["objectType"] = 2000
                #fileInfo["readModuleHeader"] = readWMDataHeader
                fileInfo["readModuleData"] = readWMData
                fileInfo["readBackgroundData"] = -1
        elif nextType==-2:
            with open(outputJSON, 'w') as f:
                json.dump(fileInfo, f)
        elif nextType==-3:
            fileInfo["ModuleHeader"] = fileInfo["readModuleHeader"](file)
        elif nextType==-4:
            fileInfo["ModuleFooter"] = fileInfo["readModuleFooter"](file,fileInfo)
    
        else:
            data = readPamData(file,fileInfo)
            print(data)
            x=1