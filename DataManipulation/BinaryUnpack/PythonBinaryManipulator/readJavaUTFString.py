from PamBinRead import pamBinRead

def readJavaUTFString(fid):
    length = pamBinRead(fid,'int16',n=1)
    str = pamBinRead(fid,'character',n=length)
    return str