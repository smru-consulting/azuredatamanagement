from PamBinRead import pamBinRead

def readStdModuleFooter(file):
    footer = {}
    footer['length'] = pamBinRead(file,'int32',n=1)
    footer['binaryLength'] = pamBinRead(file,'int32',n=1)
    return footer