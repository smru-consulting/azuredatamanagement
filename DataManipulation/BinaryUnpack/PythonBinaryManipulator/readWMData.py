from os import read
from PamBinRead import pamBinRead
import statistics

def readWMData(file,headerData,chunkData,skipLarge):
    
    data={}
    print("ENTERED")
    dataLength = pamBinRead(file,'int32',n=1)
    startPos = file.tell()
    version = 1#headerData['version']
    readLength = 0


    if int(version)<=1:
        data['startSample'] = pamBinRead(file,'int64',n=1)
        readLength+=8
        data["channelMap"] = pamBinRead(file,'int32',n=1)
        readLength+=4

    data['nSlices']=pamBinRead(file,'int16',n=1)
    readLength+=2

    if int(version)>=1:
        data['amplitude'] = pamBinRead(file,'int16',n=1)/100
        readLength+=2

    if int(version)==1:
        data["nDelays"] = pamBinRead(file,'int8',n=1)
        readLength+=1
        data["delays"] = []
        for i in range(data['nDelays']):
            data['delays'].append(pamBinRead(file,'float',n=1))
            readLength+=2

    if skipLarge:
        file.seek(startPos + dataLength,0)
        return data
        
    data["sliceData"] = []
    data["contour"] = [0] *data['nSlices']
    data["contWidth"] = [0] *data['nSlices']

    for i in range(data['nSlices']):
        aSlice = {}
        aSlice['sliceNumber'] = pamBinRead(file,'int32',n=1,seek=skipLarge)
        readLength+=4
        aSlice["nPeaks"] = pamBinRead(file,'int8',n=1)
        readLength+=1
        if aSlice["nPeaks"]<2:
            continue
        aSlice["peakData"] = [[0 for i in range(4)] for j in range(aSlice["nPeaks"])]
        for p in range(aSlice["nPeaks"]):
            for s in range(4):
                sss = pamBinRead(file,'int16',n=1,seek=skipLarge)
                readLength+=2
                aSlice["peakData"][p][s]=sss
        data["sliceData"].append(aSlice)
        data["contour"][i] = 0#aSlice["peakData"][2][1]
        data["contWidth"][i] = 0#aSlice["peakData"][3][1] - aSlice["peakData"][1][1] + 1
    
    if data["nSlices"]>0:
        data["meanWidth"] = statistics.mean(data["contWidth"])
    else:
        data["meanWidth"] = 0

    return data