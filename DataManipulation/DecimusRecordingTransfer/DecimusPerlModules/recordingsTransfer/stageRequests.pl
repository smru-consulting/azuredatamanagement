#!/usr/bin/perl

=head1 execute::recordingsTransfer::stageRequests

NA

=cut

package recordingsTransfer::stageRequests;

use lib '/home/root/pambuoy/current/modules';
use xmlProcessing::xmlReadWrite;
use serverSideDataRecovery::dataManagerMethods::settingsProcessing;
use generic::dataManagment::hashManagment;
use SmruLogging;
use generic::sysManagment::sysCommands;

my $requestQueueLocation = '/media/sdb1/buoydata/recordings/queueIndex.xml';
my $recordingBaseLoc = '/media/sdb1/buoydata/recordings/318/';
my $recordingQueueDestBase = '/media/sdb1/buoydata/recordings/sendQueue/';
%myHash;

loadXmlFileOntoHashNewKeysAllowed($requestQueueLocation,\%myHash);

processHash(%myHash);

sub processHash{
	my(%requestHash) = @_;
	smruLogOut 1, "Staging recordings for sending";
	for my $key (keys %{$requestHash{requests}}){
		##print $key."\n";
		if($requestHash{requests}{$key}->{staged}==0){
			smruLogOut 10, "Staging recordings request ".$requestHash{requests}{$key}->{dateStr}." ".$requestHash{requests}{$key}->{startTime}."-".$requestHash{requests}{$key}->{endTime}." for sending";
			processRequest($requestHash{requests}{$key});
		}
	}
	
}

sub processRequest{
	my($request) = @_;
	my $datestr = $request->{dateStr};
	my $startTimeStr = $request->{startTime};
	my $endTimeStr = $request->{endTime};

	my $startInt = int($startTimeStr);
	my $endInt = int($endTimeStr);
	my @minuteRequests = ($startInt..$endInt);
	
	$recordingQueueDest = $recordingQueueDestBase.$datestr."/.";
	
	executeSystemCommand("mkdir $recordingQueueDestBase$datestr" );
	
	my @files;
	foreach my $thisTime (@minuteRequests){
		my $wildCardSearch;
		#print $thisTime;
		if($thisTime<100){
			#print " <100\n";
			$wildCardSearch = $recordingBaseLoc.$datestr."/*_".$datestr."_00".$thisTime."*.x3a";
		}elsif($thisTime<1000){
			#print " <1000\n";
			$wildCardSearch = $recordingBaseLoc.$datestr."/*_".$datestr."_0".$thisTime."*.x3a";
		}else{
			#print " >=1000\n";
			$wildCardSearch = $recordingBaseLoc.$datestr."/*_".$datestr."_".$thisTime."*.x3a";
		}
		#print $wildCardSearch;
		@files = glob($wildCardSearch);
		foreach my $filename (@files){
			##print $filename."\n";
			my $command = "cp ".$filename." ".$recordingQueueDest;
			print $command."\n";
			executeSystemCommand($command);
			smruLogOut 1, "Staging ".$filename." for sending";
		}
	}
	$request->{staged}=1;
	##print $request->{staged}."\n";
}

hash2xmlFile(\%myHash,"Buoy",$requestQueueLocation);
