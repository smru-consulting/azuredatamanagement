#Script to parse the executable summary status update for a buoy. 
#This line of status update contains the high level information from the PGLite ASP on Decimus
#It is rooted in the C++ pamguard status in the network section of code. Yet to be determined exactly how this is collected on the remote end
#TODO: ACTUALLY PARSE THESE DATA. Figure out how to integrate the correct frequency domains
#For detectors, the data are partitioned into frequency bins for the update. Example:
#   Moans:4,0,0,0,0
#   This indicates that for the module named "Moans" there are four frequency bins reported, followed by the detection count for each of the four bins

import re

def ParseExeSummary(EXESUMMARY,deployment):
    ASPModules = deployment["ASPModules"]
    ASPStatus = {}
    for module in ASPModules:
        if module=="Moans":
            ASPStatus[module] = getMoansCounts(EXESUMMARY)
        elif module=="Noise":
            ASPStatus[module] = getNoiseMeasure(EXESUMMARY)
    return ASPStatus

def getMoansCounts(EXESUMMARY):
    counts = {}
    moansReport = re.search(r"Moans:[0-9]+,([0-9]+),([0-9]+),([0-9]+),([0-9]+)",EXESUMMARY)
    counts["0-7.8kHz"] = int(moansReport.group(1))
    counts["7.8-15.6kHz"] = int(moansReport.group(2))
    counts["15.6-23.4kHz"] = int(moansReport.group(3))
    counts["23.4-31.2kHz"] = int(moansReport.group(4))
    return counts

def getNoiseMeasure(EXESUMMARY):
    measures = {}
    noiseReport = re.search(r"Noise:\w+,\w+,[0-9]+,[0-9]+,([0-9]+.[0-9]+),([0-9]+.[0-9]+),([0-9]+.[0-9]+)",EXESUMMARY)
    print(noiseReport)
    measures["z2p"] = float(noiseReport.group(1))
    measures["p2p"] = float(noiseReport.group(2))
    measures["rms"] = float(noiseReport.group(3))
    return measures
