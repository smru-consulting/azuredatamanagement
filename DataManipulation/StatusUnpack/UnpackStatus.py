#Module designed to gather file-stored status variables and compile into MongoDB document
#Status are collected on the Decimus and sent to zDataStore. They are immediately parsed into perl hash in /home/decimus1/zDataStore/pb318/repo/statusCombined
#   as well as individual status variables in /home/decimus1/zDataStore/pb318/statusDir
#All reporting done in the status files are reflective either the instant of report, or in the case of detectors since the last report was made.


from os import times_result
from datetime import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from ParseExeSummary import ParseExeSummary

def unpackStatus(deployment):
    #Define the base path for status directory
    BuoyID = deployment["BuoyID"]
    statusDir = "C:/AzureDevelopment/AzureDataManagement/StaticData/zDataStore/"+BuoyID+"/statusDir/"

    statusObj = {}

    statusObj["Deployment"] = {"$ref":"Deployments","$id":ObjectId(deployment["_id"]),"$db":"SMRUDataStore"}

    #Get the time of status update
    with open(statusDir+"TIME",'r') as f:
        timestr = f.read()
        timestr = timestr.split('TIME ')[1]
        statustime  = datetime.strptime(timestr,'%Y/%m/%d %H:%M:%S')
        statusObj["UTC"] = statustime
        f.close()
    
    #Get the time of the last status update... Seems to be the same as TIME
    with open(statusDir+"LASTSTATUSTIME") as f:
        dtString = f.read()
        lastDt = datetime.strptime(dtString,"%Y%m%d%H%M%S")
        statusObj["LastStatusUTC"] = lastDt
        f.close()
    
    #Get voltage
    with open(statusDir+"BATTVOLTAGE",'r') as f:
        vstring = f.read()
        voltage = float(vstring.split("V")[0])
        statusObj["Voltage"] = voltage
        f.close() 

    #Get system state
    with open(statusDir+"CURSTATE") as f:
        statusObj["CurrentState"] = f.read()
        f.close()

    #Get ASP Summary
    with open(statusDir+"EXESUMMARY") as f:
        statusObj["ASPStatus"] = ParseExeSummary(f.read(),deployment)
        f.close()

    #Get retry elements
    with open(statusDir+"RETRYELEMENTS") as f:
        statusObj["RetryElements"] = int(f.read())
        f.close()
    
    print(statusObj)
    return statusObj

def commitStatusToDB(statusObject):
    client = MongoClient(port=27017)
    db = client["SMRUDataStore"]
    Status = db["Status"]
    Status.insert_one(statusObject)

client = MongoClient(port=27017)
db = client["SMRUDataStore"]
Deployments = db["Deployments"]
thisDeployment = Deployments.find_one({'BuoyID':'pb318','Active':True})
commitStatusToDB(unpackStatus(thisDeployment))