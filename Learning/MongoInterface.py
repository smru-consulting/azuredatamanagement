from pymongo import MongoClient

client = MongoClient(port=27017)
db = client.SMRUDataStore

db.Deployments.insert_one({"BuoyID":"pb325",
                        "LocationName":"Puget Sound",
                        "lat":48.35321,
                        "lon":-123.13561,
                        "deploymentDate":"2021/12/7 19:00:00",
                        "dTypes":["dataWM","adc"],
                        "access":["SMRUadmin"],
                        "subscribers":["st@smruconsulting.com","samtabbutt@gmail.com"],
                        "thetaOff":2.25147283})