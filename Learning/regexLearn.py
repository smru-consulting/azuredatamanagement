import re

testStr = "datetime(2022,1-1,1,17,34,00),datetime(2021,11,31,22,12,15)"

easier = "sam(harris,2), sam(waller,3), duncan"

def evalMonth(match):
    monthStr = match.group('month')
    if '-' in monthStr:
        monthInt=eval(monthStr)
    else:
        monthInt=int(monthStr)
    monthInt+=1
    returnStr = "datetime("+match.group(1)+","+str(monthInt)+","+match.group(3)+","+match.group(4)+","+match.group(5)+","+match.group(6)+")"
    return returnStr

m = re.sub(r"datetime\(([0-9]{4}),(?P<month>[\w.-]+),([\w.-]+),([\w.-]+),([\w.-]+),([\w.-]+)\)",evalMonth,testStr)

print(m)