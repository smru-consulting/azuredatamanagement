from abc import ABC, abstractmethod

class ComponentMonitor(ABC):

    def __init__(self,buoyMeta,timeRange,args=0):
        self.buoyMeta = buoyMeta
        self.timeRange = timeRange
        self.args = args

    @abstractmethod
    def isInAlertState(self):
        pass

    @abstractmethod
    def initAlertLogic(self):
        pass

#    @abstractmethod
#    def sendAlertEmail(self):
#        pass

    