import sys
from datetime import datetime
from datetime import timedelta
import pandas as pd
from ComponentMonitor.AbstractMonitor import ComponentMonitor
sys.path.append('../')
from Utilities import buoyDataAccess
from Utilities import emailHandling

#Detector monitor object for a given buoy with buoyMeta, an associated jsonbasepath (todo: work into buoyMeta), and the timeframe for monitoring all detectors on the buoy
class DetectionMonitor(ComponentMonitor):
    #Constructor params:
    #   buoyMeta: same buoyMeta for any buoy in the system
    #   jsonbathpath: base path for the json store for the buoy (/path/to/buoy)
    #   timeFrame: array of length 2 with start and end time of monitor as python datetime objects
    def __init__(self,buoyMeta,timeRange):
        super().__init__(buoyMeta,timeRange)
        self.jsonbasepath = "C:/AzureDevelopment/AzureDataManagement/StaticData/sampleJSON/projects/SmruLlc/"+self.buoyMeta["ID"].split("pb")[1]
        #A list containing one entry for each detector high level data set in JSON files
        #Each detector has a series of frequency domains, so for one detector you'll get multiple entries here
        #Each entry contains "detectorName" (To be altered in JSON form, currently the name of the chart.. something long and dumb),
        #   "detectionCount" (the number of detections in the time frame), 
        #   and "freqDom" (the frequency domain for the count)
        self.detectorMeta = self.getdetectorMeta()
        
    #Called at constructor, returns the detectorMeta object
    def getdetectorMeta(self):
        detectorMeta = []
        for dType in self.buoyMeta['dTypes']:
            if dType=="adc":
                continue
            detectorData,dataName=buoyDataAccess.jsonToDataFrame(self.jsonbasepath,dType)
            searchStartTime = self.timeRange[0]
            searchEndTime = self.timeRange[1]
            checkRegionData = pd.DataFrame()
            for column in detectorData.columns:
                if "TimeSeries" in column:
                    checkRegionData = detectorData[detectorData[column]>searchStartTime]
                    checkRegionData = checkRegionData[checkRegionData[column]<searchEndTime]
            if checkRegionData.empty:
                continue
            for column in checkRegionData.columns:
                if "DataSeries" in column:
                    detectorMeta.append({'detectorName':dataName,
                                        'detectionCount':checkRegionData[column].sum(),
                                        'freqDom':column.split('DataSeries')[0]})
        return detectorMeta

    #Returns boolean. Are there any detections in the specified timeframe
    def isInAlertState(self):
        for detector in self.detectorMeta:
            if detector['detectionCount']>0:
                return True
        return False
    
    #Given a pandas dataframe with column UTC, returns the densist 1/60th portion of the data
    def getDensistMinute(self,moansTable):
        moansTable['outbin'] = pd.cut(moansTable["UTC"],60)
        groupedMoansTable = moansTable.groupby('outbin').count()
        maxBin = groupedMoansTable.idxmax()
        return [maxBin[0].left,maxBin[0].right],groupedMoansTable.max()[0]
        
    #called externally, this function will get a table from the appropriate detection database, determine the densist minute of detections,
    #and submit a recording request to the unit
    def initAlertLogic(self):
        moansTable = buoyDataAccess.getDatabaseTable(self.buoyMeta,'Moans',self.timeRange)
        if not moansTable.empty:
            recordingRequestTimeBounds,count = self.getDensistMinute(moansTable)
            buoyDataAccess.submitRecordingRequest(self.buoyMeta,recordingRequestTimeBounds)
        else:
            timeBounds,count = [0,0],0
        self.sendAlertEmail(timeBounds,count)
        return

    #To be written, this function will package the recordings on the server in self.timeFrame, post to a download http:// page, and return the url of that page
    def getRecordingsDownloadPath(self):
        x=1
        return "https://google.com"

    #This function sends an email containing all detection data in self.detectorMeta
    def sendAlertEmail(self,requestedRecordings,count):
        recievers = self.buoyMeta['subscribers']
        subject = "Testing Detection Event for "+self.buoyMeta['ID']+" Deployed at "+self.buoyMeta['LocationName']
        html = "<div><strong>A detection has occured on one or more detectors on %s at %s.</strong><p>Detection Data:</p>" %(self.buoyMeta['ID'],self.buoyMeta['LocationName'])
        for detector in self.detectorMeta:
            html+="<p>%s: %d counts between %s and %s UTC in frequency domain: %s</p>" %(detector['detectorName'],detector['detectionCount'],self.timeRange[0],self.timeRange[1],detector['freqDom'])
        html+="<p>The densist minute of detections started at %s UTC with %d detections. A raw recording has been requested for this minute.</p>" %(requestedRecordings[0],count)
        html+="<p>You can download the recordings here: <a href=%s>%s</a></p>" %(self.getRecordingsDownloadPath(),self.getRecordingsDownloadPath())
        html+="</div>"
        emailHandling.sendMessage(recievers,subject,html)
        return