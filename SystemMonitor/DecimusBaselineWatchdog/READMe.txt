Script task: to notify subscribers of a buoy if data has not been sent for over an hour
Schedule: TBD
Process: scan buoy status hash table for datetime of last data transfer and PG status. If either variable violates pass condition, send alert email to the subscribers.
Variables: Schedule routine, data transfer lag time threshold, subscribers email address. 
Tools: Azure sendgrid, python