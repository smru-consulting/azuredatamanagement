import sys
from datetime import datetime
from datetime import timedelta
sys.path.append('../')
from Stability.stability import StabilityMonitor
from ComponentMonitor.DetectionMonitor import DetectionMonitor
from Utilities import emailHandling
from Utilities import buoyDataAccess

#Set default parameters
BUOYID=""
JSONBASEPATH=""
ALERTBINTIME=1
ALERTLOGTIME=24
ALERTRECTIME=0
ALERTDETTIME=110

#ARGUMENTS
for (idx,argument) in enumerate(sys.argv):
    #Buoy number (without "pb")
    #REQUIRED
    if argument=="-BUOYID":
        BUOYID = sys.argv[idx+1]
    #Base path for JSON data for the buoy
    #REQUIRED
    elif argument=="-JSONBASEPATH":
        JSONBASEPATH=sys.argv[idx+1]
    #Time threshold for missing binary data to notify subscribers
    #Default is 0, if no input for parameter binary data will be ignored in data lag process
    elif argument=="-ALERTBINTIME":
        ALERTBINTIME=float(sys.argv[idx+1])
    #Time threshold for missing log data to notify subscribers
    #Default is 0, if no input for parameter system logs will be ignored in data lag process
    elif argument=="-ALERTLOGTIME":
        ALERTLOGTIME=float(sys.argv[idx+1])
    #Time threshold for missing recording data to notify subscribers
    #Default is 0, if no input for parameter recordings will be ignored in data lag process
    elif argument=="-ALERTRECTIME":
        ALERTRECTIME=float(sys.argv[idx+1])
    #Length of data to scan for detetections
    #Default is 1 hour
    elif argument=="-ALERTDETTIME":
        ALERTDETTIME=float(sys.argv[idx+1])

#Generate dict of data lag thresholds
ALERTTIMES = {'recsTimeSeries':ALERTRECTIME,'sysLogsTimeSeries':ALERTLOGTIME,'binDataTimeSeries':ALERTBINTIME}


#Gather buoy meta data for deployment
buoyMeta = buoyDataAccess.getBuoyMeta(BUOYID)
if(buoyMeta==-1):
    print("Response 401 Error in retrieving buoy metadata")
    quit()


stabilityMonitor = StabilityMonitor(buoyMeta,JSONBASEPATH,ALERTTIMES)
#If a stream's last data sent exceeds the determined threshold send a notification email to the buoy's subscribers
if(not stabilityMonitor.isDataStable()):
    stabilityMonitor.sendDataLagEmail()

DETECTIONNEWTIMES = [datetime.utcnow()-timedelta(hours=ALERTDETTIME),datetime.utcnow()]
detectionMonitorNew = DetectionMonitor(buoyMeta,DETECTIONNEWTIMES)
if(detectionMonitorNew.isInAlertState()):
    requestedTimeBounds,count = detectionMonitorNew.initAlertLogic()
    
#DETECTIONOLDTIMES = [datetime.utcnow()-timedelta(hours=ALERTDETTIME+1),datetime.utcnow()-timedelta(hours=1)]
#detectionMonitorOld = DetectionMonitor(buoyMeta,DETECTIONOLDTIMES)
#if(detectionMonitorOld.isNewDetections()):
#    recordingsDownloadPath = detectionMonitorOld.getRecordingsDownloadPath()
#    requestedTimeBounds,count = detectionMonitorNew.handleRecordingsRequest(False)
#    detectionMonitorOld.sendEmail(requestedTimeBounds,count,recordingsDownloadPath)