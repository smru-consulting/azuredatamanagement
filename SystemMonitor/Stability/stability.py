from datetime import datetime
from datetime import timedelta
import sys
sys.path.append('../')
from Utilities import buoyDataAccess
from Utilities import emailHandling

class StabilityMonitor:
    def __init__(self,buoyMeta,jsonbasepath,alertTimes):
        self.buoyMeta = buoyMeta
        self.jsonbasepath = jsonbasepath
        self.alerttimes = alertTimes
        self.dataSendStatus = self.getBuoyStabilityData()
        

    def getBuoyStabilityData(self):
        #Get Data send status for threshold parameters
        dvolData,dataName =buoyDataAccess.jsonToDataFrame(self.jsonbasepath,'dvol')
        dataSendStatus = self.getDataSendStatus(dvolData)
        return dataSendStatus

    #Fuction getDataSendStatus
    #Parameter: timeSeriesDF: pandas dataframe consisting of two columns for each data stream: a TimeSeries and a DataSeries
    #                         form is output from Utilities.buoyDataAccess.jsonToDataFrame
    #Returns: Dict containing one key for each data stream as streamName+"TimeSeries". The value for each stream key is a dict with 'upToDate' (boolean) and 'lastDataSent' (datetime)
    def getDataSendStatus(self,timeSeriesDF):
        dataSendStatus={}
        for column in timeSeriesDF.columns:
            if "DataSeries" in column:
                continue
            if self.alerttimes[column]>0:
                upToDate = (datetime.utcnow()-timeSeriesDF[column].max())<timedelta(hours=self.alerttimes[column])
            else:
                upToDate = True
            dataSendStatus[column] = {'upToDate':upToDate,'lastDataSent':timeSeriesDF[column].max()}
        return(dataSendStatus)

    def isDataStable(self):
        for dataStream in self.dataSendStatus.keys():
            if self.dataSendStatus[dataStream]['upToDate']==False:
                return False
        return True

    def getVoltageReading(self):
        adcData,dataName = buoyDataAccess.jsonToDataFrame(self.jsonbasepath,'adc')
        voltMaxIdx = adcData['BatteryVoltageTimeSeries'].idxmax()
        return adcData['BatteryVoltageDataSeries'].iloc[voltMaxIdx]

    def sendDataLagEmail(self):
        recievers = self.buoyMeta['subscribers']
        subject = "Testing Data Lag for "+self.buoyMeta['ID']+" Deployed at "+self.buoyMeta['LocationName']
        html = "<div><strong>The Data Lag Threshold has been exceeded for one or more data streams.</strong><p>Last Data Recieved:</p>"
        for dataStream in self.dataSendStatus.keys():
            html+="<p>%s: %s UTC</p>" %(dataStream.split("TimeSeries")[0],self.dataSendStatus[dataStream]['lastDataSent'])
        html+="</div>"
        emailHandling.sendMessage(recievers,subject,html)