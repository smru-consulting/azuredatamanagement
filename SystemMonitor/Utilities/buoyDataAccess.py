import sys
import glob
import pandas as pd
import numpy as np
from datetime import datetime, time, timedelta
import json
import sqlite3
import xml.etree.ElementTree as ET
import re

def evalMonth(match):
    monthStr = match.group('month')
    if '-' in monthStr:
        monthInt=eval(monthStr)
    else:
        monthInt=int(monthStr)
    monthInt+=1
    returnStr = "datetime("+match.group(1)+","+str(monthInt)+","+match.group(3)+","+match.group(4)+","+match.group(5)+","+match.group(6)+")"
    return returnStr

#Function dvolPathToDF
#param: the base path for buoy data in JSON format
#returns: pandas dataframe with a column for each data stream and the times of data transfer
def jsonToDataFrame(parentDir,dataType):
    #Grab all files for the desired datatype
    dvol_files = glob.glob(parentDir+"/*"+dataType+"*")
    #grab the first match
    dvol_path = dvol_files[0]
    dvol_f = open(dvol_path)
    jsonpString = dvol_f.read()
    #The "JSON" output from the perl script is not actually JSON, its JSONP, or just Javascript code
    #We have to take javascript code and format it as python code. A slight hack to work with the output from perl scripts
    jsonpString = jsonpString.replace("Date.UTC","datetime")
    #normalize without new lines
    jsonpString = jsonpString.replace("\n","")
    #get rid of spaces
    jsonpString = jsonpString.replace(" ","")
    #Get rid of leading zeroes
    jsonpString = jsonpString.replace(",0",",")
    #Fix everything that shouldn't have been removed when removing leading zeroes
    jsonpString = jsonpString.replace(",]",",0]")
    jsonpString = jsonpString.replace(",}",",0}")
    jsonpString = jsonpString.replace(",)",",0)")
    jsonpString = jsonpString.replace(",,",",0,")
    jsonpString = jsonpString.replace("\'","\"")
    #put categories key and data key as strings
    jsonpString = jsonpString.replace(",categories",",\"categories\"")
    jsonpString = jsonpString.replace(",data",",\"data\"")
    #Javascript takes values[0,11] for month parameter, python takes [1,12]. Evaluate if JSONP contains subrataction and add a month to every date
    jsonpString = re.sub(r"datetime\(([0-9]{4}),(?P<month>[\w.-]+),([\w.-]+),([\w.-]+),([\w.-]+),([\w.-]+)\)",evalMonth,jsonpString)

    #evaluate as python code!
    jsonOut = eval(jsonpString)
    #Throw it into a dataframe
    timeSeriesDF = pd.DataFrame()
    for series in jsonOut[0]["series"]:
        dataND = np.array(series["data"])
        timeSeriesDF[series["name"]+"TimeSeries"] = dataND[:,0]
        timeSeriesDF[series["name"]+"DataSeries"] = dataND[:,1]
    
    return timeSeriesDF,jsonOut[0]['title']

#Function to get buoy metadata object. Currently exists as a JSON file (format found in /home/decimus1/www/clientarea/SmruLlc/projects/activeBuoys.JSON)
#Param: buoyID in format "pb318" as string
#Returns: buoyMeta as dict
def getBuoyMeta(buoyID):
    activeBuoysJsonLoc = "C:/AzureDevelopment/AzureDataManagement/StaticData/sampleJSON/projects/activeBuoys.json"
    activeBuoysJsonFile = open(activeBuoysJsonLoc)
    activeBuoys = json.load(activeBuoysJsonFile)
    #try to get the buoy metadata from the active buoys list
    try:
        buoyMeta = activeBuoys['activeBuoyList'][buoyID]
    #return -1 if something goes wrong
    except:
        buoyMeta = -1
    return(buoyMeta)

#Function to get time restriced data from sqlite3 database. 
#Params: buoyMeta: currently in the format in (/home/decimus1/www/clientarea/SmruLlc/projects/activeBuoys.JSON). 
#                   Get buoyMeta in correct format by using buoyDataAccess.getBuoyMeta(pbID)
#       tableName: name of table in SQLite database to query. e.g. "Moans"
#       timeResctrictions: array of length two with python datetime objects defining lower and upper bounds of query respectively
#Returns: Pandas dataframe of the SQLite table with the time restrictions 
def getDatabaseTable(buoyMeta,tableName,timeResctriction):
    sqliteLoc = 'C:/AzureDevelopment/AzureDataManagement/StaticData/sampledb/pb318_PTROB_02.sqlite3'
    con = sqlite3.connect(sqliteLoc)
    starttime = timeResctriction[0].strftime("%Y-%m-%d %H:%M:%S.%f")
    endtime = timeResctriction[1].strftime("%Y-%m-%d %H:%M:%S.%f")
    QUERY = '''SELECT UTC FROM '''+tableName+" WHERE UTC BETWEEN \'"+starttime+"\' AND \'"+endtime+"\'"
    df = pd.read_sql_query(QUERY, con)
    df['UTC'] = pd.to_datetime(df["UTC"])
    return df

#Function to submit a recording request for a specific buoy
#Params: buoyMeta: the buoyMeta for a buoy. Get buoy meta by using buoyDataAccess.getBuoyMeta(pbID)
#        timeFrame: array of length two with python datetime objects defining lower and upper bounds of query respectively
#WARNING: This function modifies an XML file which is queried by the remote unit. If the request for recordings is overwhelming, the remote unit will become unstable
def submitRecordingRequest(buoyMeta,timeFrame):
    dateStr = timeFrame[0].strftime("%Y%m%d")
    StartTime = timeFrame[0].strftime("%H%M")
    timeFrame[1] = timeFrame[1]+timedelta(minutes=1)
    EndTime = timeFrame[1].strftime("%H%M")
    print(dateStr,StartTime,EndTime)
    buoyRestQueueLoc = "C:/Users/12066/Documents/WebAppDev/CABOW Web Viewer/php/sampleJSON/deviceData/"+buoyMeta["ID"]+"/recordingsRequest/queue.xml"
    tree = ET.parse(buoyRestQueueLoc)
    root = tree.getroot()
    for request in root.iter('request'):
        if (request.attrib['dateStr']==dateStr and request.attrib['startTime']==StartTime and request.attrib['endTime']==EndTime):
            return
    for child in root:
        newRequest = ET.SubElement(child,'request')
        newRequest.set('dateStr',dateStr)
        newRequest.set('endTime',EndTime)
        newRequest.set('staged','0')
        newRequest.set('startTime',StartTime)
    with open(buoyRestQueueLoc, 'wb') as f:
        tree.write(f, encoding='utf-8')